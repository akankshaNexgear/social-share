//
//  ViewController.m
//  Social Share
//
//  Created by Apple on 03/08/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

#import "ViewController.h"
#import<MobileCoreServices/UTCoreTypes.h>
#import <MediaPlayer/MediaPlayer.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)share:(id)sender {
    
    //NSURL *url = [NSURL URLWithString:@"http://getsharekit.com"];
    NSURL *url = [NSURL URLWithString:@"/Users/apple/Downloads/instagram03140.mp4"];
    
    //SHKItem *item = [SHKItem filePath:@"/Users/apple/Downloads/instagram03140.mp4" title:@"Instagram Video"];
    SHKItem *item = [SHKItem URL:url title:@"ShareKit is Awesome!" contentType:SHKURLContentTypeVideo];
    [SHKFacebook shareItem:item];
    //[SHK addToOfflineQueue:item forSharer: NSStringFromClass(SHKFacebook.class)];
    //[SHK flushOfflineQueue];
    
    
    
   /* UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
    NSArray *sourceTypes = [UIImagePickerController availableMediaTypesForSourceType:imagePicker.sourceType];
    if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ])
    {
        NSLog(@"no video");
    }
    
    else
    {
        [self presentModalViewController:imagePicker animated:YES];
    }
    */
    
    
    
    
    // Create the item to share (in this example, a url)
    
    //NSURL *url = [NSURL URLWithString:@"http://getsharekit.com"];
   // NSURL *url = [NSURL URLWithString:@"/Users/apple/Downloads/instagram03140.mp4"];

    //SHKItem *item = [SHKItem filePath:@"/Users/apple/Downloads/instagram03140.mp4" title:@"Instagram Video"];
    //SHKItem *item = [SHKItem URL:url title:@"ShareKit is Awesome!" contentType:SHKURLContentTypeVideo];
    //[SHKFacebook shareItem:item];
    //[SHK addToOfflineQueue:item forSharer: NSStringFromClass(SHKFacebook.class)];
    //[SHK flushOfflineQueue];
    // Display the action sheet
    /*if (NSClassFromString(@"UIAlertController")) {
        
        //iOS 8+
        SHKAlertController *alertController = [SHKAlertController actionSheetForItem:item];
        [alertController setModalPresentationStyle:UIModalPresentationPopover];
        UIPopoverPresentationController *popPresenter = [alertController popoverPresentationController];
        popPresenter.barButtonItem = self.toolbarItems[1];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else {
        
        //deprecated
        SHKActionSheet *actionSheet = [SHKActionSheet actionSheetForItem:item];
        [actionSheet showFromToolbar:self.navigationController.toolbar];
    }*/
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:(NSString *)kUTTypeVideo] || [type isEqualToString:(NSString *)kUTTypeMovie])
    {
        NSURL *urlvideo = [info objectForKey:UIImagePickerControllerMediaURL];
        SHKItem *item = [SHKItem URL:urlvideo title:@"test video!" contentType:SHKURLContentTypeVideo];
       // [SHKFacebook shareItem:item];
        item.shareType = SHKShareTypeURL;
        [SHKSharer shareURL:urlvideo title:@"test Video"];

        //[SHKSharer shareFileData:[NSData dataWithContentsOfURL:urlvideo] filename:@"testVideo.mp4" title:@"testVideo"];
        //SHKiOSFacebook *fbShare = [[SHKiOSFacebook alloc] init];
        //fbShare.item = item;
        //[SHKiOSFacebook send];
    }
}
- (IBAction)openActivityShare:(id)sender {
    NSString *textToShare = @"Look at this awesome website for aspiring iOS Developers!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.codingexplorer.com/"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)performActivityShare:(id)sender {

    FBActivity *fbActivity = [[FBActivity alloc] init];
    NSString *textToShare = @"Look at this awesome website for aspiring iOS Developers!";
    NSString *myWebsite = @"Sample Text";
    //[NSURL URLWithString:@"http://www.codingexplorer.com/"];
    NSArray *objectsToShare = @[textToShare, myWebsite];
    [fbActivity prepareWithActivityItems:objectsToShare];
    [fbActivity performActivity];
}

- (IBAction)facebookShare:(id)sender {
    /*UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    imagePicker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
    NSArray *sourceTypes = [UIImagePickerController availableMediaTypesForSourceType:imagePicker.sourceType];
    if (![sourceTypes containsObject:(NSString *)kUTTypeMovie ])
    {
        NSLog(@"no video");
    }
    
    else
    {
        [self presentModalViewController:imagePicker animated:YES];
    }*/
    [SHKFacebook shareFileData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"instagram03140" ofType:@"mp4"]] filename:@"instagram03140.mp4" title:@"test Video"];
    //[SHKFacebook shareFilePath:@"/Users/apple/Downloads/instagram03140.mp4" title:@"test video"];

}
- (IBAction)youtubeShare:(id)sender {
    BAHYouTubeOAuth *youTubeOAuth = [[BAHYouTubeOAuth alloc]init];
    
    [youTubeOAuth authenticateWithYouTubeUsingYouTubeClientID:@"348134387519-tqsouuj9hfon405rhohfalhqlc1lmpi0.apps.googleusercontent.com"
                                          youTubeClientSecret:@"ZzWDgnMCBlMA05Q8vKneD8Lk"
                                                 responseType:@"code"
                                                        scope:@"https://www.googleapis.com/auth/youtube.force-ssl%20https://www.googleapis.com/auth/youtube%20https://www.googleapis.com/auth/youtubepartner%20https://www.googleapis.com/auth/youtube.upload"
                                                        state:@""
                                               appURLCallBack:@"urn:ietf:wg:oauth:2.0:oob"
                                                   accessType:@"offline"
                                               viewController:self
                                                             :^(BOOL success, NSString *youTubeToken, NSString *youTubeRefreshToken) {
                                                                 
                                                                 if (success) {
                                                                     //the token you will use to request right now
                                                                     [[NSUserDefaults standardUserDefaults] setObject:youTubeToken forKey:@"youtube_token"];
                                                                     //token you can use to request a new token on your behalf for requestion later
                                                                     //this only shows when you ask for "offline access"
                                                                     [[NSUserDefaults standardUserDefaults] setObject:youTubeRefreshToken forKey:@"youtube_refresh"];
                                                                     
                                                                     [[NSUserDefaults standardUserDefaults] synchronize];
                                                                     NSLog(@" youtube token %@",youTubeToken);
                                                                     NSLog(@" youtube refresh token %@",youTubeRefreshToken);
                                                                     //Do whatever you need with the token
                                                                     
                                                                 }
                                                                 
                                                                 
                                                            }];
    
}

- (NSString *) getCorrectURL{
    
    //return [[NSString alloc] initWithFormat:@"https://www.googleapis.com/upload/youtube/v3/videos"];
    
    
    return [[NSString alloc] initWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=snippet,status&key=%@&access_token=%@",@"AIzaSyAgcGf7jXXNe5Pk_PohUv2tV-kjdUI1TPY",[[NSUserDefaults standardUserDefaults] objectForKey:@"youtube_token"]];
}

- (NSString *) getURL{
    
   //return [[NSString alloc] initWithFormat:@"https://www.googleapis.com/upload/youtube/v3/videos"];
    

  //  return [[NSString alloc] initWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=snippet,status&key=%@&access_token=%@",@"AIzaSyAgcGf7jXXNe5Pk_PohUv2tV-kjdUI1TPY",[[NSUserDefaults standardUserDefaults] objectForKey:@"youtube_token"]];
  return [[NSString alloc] initWithFormat:@"https://www.googleapis.com/upload/youtube/v3/videos?part=snippet,status&key=%@&access_token=%@",@"AIzaSyAgcGf7jXXNe5Pk_PohUv2tV-kjdUI1TPY",[[NSUserDefaults standardUserDefaults] objectForKey:@"youtube_token"]];
}

- (NSString *) getUpdateURL{
    
   // return [[NSString alloc] initWithFormat:@"https://www.googleapis.com/youtube/v3/videos"];
    
    
   return [[NSString alloc] initWithFormat:@"https://www.googleapis.com/youtube/v3/videos?part=id,snippet&key=%@&access_token=%@",@"AIzaSyAgcGf7jXXNe5Pk_PohUv2tV-kjdUI1TPY",[[NSUserDefaults standardUserDefaults] objectForKey:@"youtube_token"]];
}
- (IBAction)uploadYoutubeVideo:(id)sender {
    NSLog(@"Request: %@", [self getURL]);

 AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
 //NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:@"AIzaSyAgcGf7jXXNe5Pk_PohUv2tV-kjdUI1TPY",@"key",@"snippet,status",@"part",@"resumable",@"uploadType", nil];
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *snippetDictionary = [[NSMutableDictionary alloc] init];
    [snippetDictionary setObject:@"Frodo" forKey:@"title"];
    [snippetDictionary setObject:@"Frodo rocks" forKey:@"description"];
    [snippetDictionary setObject:@"22" forKey:@"categoryId"];
    [snippetDictionary setObject:[NSArray arrayWithObjects:@"Frodo",@"Rocks", nil] forKey:@"tags"];
    
    [body setObject:snippetDictionary forKey:@"snippet"];
    
   NSMutableDictionary *statusDictionary = [[NSMutableDictionary alloc] init];
    [statusDictionary setObject:@"private" forKey:@"privacyStatus"];
    
    [body setObject:statusDictionary forKey:@"status"];
    [manager POST:[self getURL] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    [formData appendPartWithFileData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"instagram03140" ofType:@"mp4"]]
                                    name:@"mediaBody"
                                fileName:@"Frodo" mimeType:@"video/mp4"];
        NSError *error;
 
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];

        NSAssert(jsonData, @"Failure building JSON: %@", error);
        NSLog(@"BODY :: %@",body);
       // NSDictionary *jsonHeaders = [[NSDictionary alloc] initWithObjectsAndKeys:@"application/json; charset=utf-8",@"Content-type", nil];
        
        //NSDictionary *jsonHeaders = [[NSDictionary alloc] initWithObjectsAndKeys:@"application/json; charset=utf-8",@"Content-type",[NSString stringWithFormat:@"Bearer %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"youtube_token"]],@"Authorization", nil];
        
        //[formData appendPartWithHeaders:jsonHeaders body:jsonData];
//        [formData appendPartWithFileData:jsonData name:@"" fileName:@"file.json" mimeType:@"application/json"];
        [formData appendPartWithFormData:[NSJSONSerialization dataWithJSONObject:body options:0 error:&error] name:@"snippet"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        NSLog(@"Error: %@", operation.responseString);
        //[self updateVideo:[responseObject objectForKey:@"id"]];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Error: %@", operation.responseString);
    }];
}


- (void) updateVideo: (NSString *)videoID {
    NSDictionary *headersDict = @{@"Content-Type": @"application/json", @"Accept": @"application/json", @"Authorization":[[NSUserDefaults standardUserDefaults] objectForKey:@"youtube_token"]};
//NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:@"AIzaSyAgcGf7jXXNe5Pk_PohUv2tV-kjdUI1TPY",@"key",@"snippet,status",@"part",@"resumable",@"uploadType", nil
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[self getUpdateURL]]];
    [request setHTTPMethod:@"PUT"];
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *snippetDictionary = [[NSMutableDictionary alloc] init];
    [snippetDictionary setObject:@"Frodo" forKey:@"title"];
    [snippetDictionary setObject:@"Frodo rocks" forKey:@"description"];
    [snippetDictionary setObject:@"22" forKey:@"categoryId"];
    //[snippetDictionary setObject:[NSArray arrayWithObjects:@"Frodo",@"Rocks", nil] forKey:@"tags"];
    
    [body setObject:snippetDictionary forKey:@"snippet"];
    
    //NSMutableDictionary *statusDictionary = [[NSMutableDictionary alloc] init];
    //[statusDictionary setObject:@"id" forKey:@"privacyStatus"];

    [body setObject:videoID forKey:@"id"];
    NSError *error;
    NSLog(@"BODY :: %@",body);

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];

    [request setHTTPBody:jsonData];
    [request setAllHTTPHeaderFields:headersDict];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation  setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        NSLog(@"Error : %@",operation.responseString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Error : %@",operation.responseString);

    }];
    [operation start];

}

- (IBAction)uploadYoutubeVideoDetails:(id)sender {
    NSDictionary *headersDict = @{@"Content-Type": @"video/mp4", @"Accept": @"application/json", @"Authorization":[[NSUserDefaults standardUserDefaults] objectForKey:@"youtube_token"]};
    //NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:@"AIzaSyAgcGf7jXXNe5Pk_PohUv2tV-kjdUI1TPY",@"key",@"snippet,status",@"part",@"resumable",@"uploadType", nil
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[self getURL]]];
    [request setHTTPMethod:@"POST"];
    NSMutableDictionary *body = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *snippetDictionary = [[NSMutableDictionary alloc] init];
    [snippetDictionary setObject:@"Frodo" forKey:@"title"];
    [snippetDictionary setObject:@"Frodo rocks" forKey:@"description"];
    [snippetDictionary setObject:@"22" forKey:@"categoryId"];
    [snippetDictionary setObject:[NSArray arrayWithObjects:@"Frodo",@"Rocks", nil] forKey:@"tags"];
    
    [body setObject:snippetDictionary forKey:@"snippet"];
    
    //NSMutableDictionary *statusDictionary = [[NSMutableDictionary alloc] init];
    //[statusDictionary setObject:@"id" forKey:@"privacyStatus"];
    
    //[body setObject:videoID forKey:@"id"];
    NSError *error;
    NSLog(@"BODY :: %@",body);
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body options:0 error:&error];
    
    [request setHTTPBody:jsonData];
    [request setAllHTTPHeaderFields:headersDict];

    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation  setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Response: %@", responseObject);
        NSLog(@"Response : %@",operation.responseString);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        NSLog(@"Error : %@",operation.responseString);
        
    }];
    [operation start];
    
}

@end
