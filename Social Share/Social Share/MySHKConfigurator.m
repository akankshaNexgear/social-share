//
//  MySHKConfigurator.m
//  Social Share
//
//  Created by Apple on 04/08/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

#import "MySHKConfigurator.h"

@implementation MySHKConfigurator

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSString*)facebookAppId {
    return @"1517174021943376";
}

-(NSString *)appName
{
    return @"Frodo";
}
- (NSArray*)facebookWritePermissions
{
    return [NSArray arrayWithObjects:@"publish_actions",@"offline_access", nil]; //@"offline_access",
}
- (NSArray*)facebookReadPermissions
{
    return nil; // this is the defaul value for the SDK and will afford basic read permissions
}
- (NSNumber*)allowOffline {
    return [NSNumber numberWithBool:true];
}

@end
