//
//  AppDelegate.h
//  Social Share
//
//  Created by Apple on 03/08/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHK.h"
#import "MySHKConfigurator.h"
#import "SHKFacebook.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

