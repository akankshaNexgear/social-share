//
//  FBActivity.m
//  Social Share
//
//  Created by Apple on 06/08/15.
//  Copyright (c) 2015 Apple. All rights reserved.
//

#import "FBActivity.h"

@implementation FBActivity

- (NSString *)activityType{
    return UIActivityTypePostToFacebook;
}

- (NSString *)activityTitle{
    return @"test Share on Facebook";
}

+ (UIActivityCategory)activityCategory {
    return UIActivityCategoryShare;
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems {
    return YES;
}


/*- (void)performActivity
{
    // This is where you can do anything you want, and is the whole reason for creating a custom
    // UIActivity
    NSLog(@"called");
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=yourappid"]];
    [self activityDidFinish:YES];
}
*/

@end
